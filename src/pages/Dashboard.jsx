import { EditIcon, ViewIcon } from "@chakra-ui/icons";
import { Avatar, Box, Button, Card, CardBody, CardFooter, CardHeader, Divider, Flex, HStack, Heading, SimpleGrid, Text}  from "@chakra-ui/react";
import { useLoaderData } from "react-router-dom";

export default function Dashboard() {
  const tasks = useLoaderData()

  // const boxStyles = {
  //   p:"10px",
  //   bg: "purple.400",
  //   color: "white",
  //   textAlign: "center",
  //   filter: "blur(1px)",
  //   ":hover": {
  //     color: "black",
  //     bg: "blue.200"
  //   }

  // }


  // return (
  //   <Container as="section" maxW="4xl" py="20px">
  //     <Heading my="30px" p="10px">Chakra UI Components</Heading>
  //     <Text ml="30px">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere, labore.</Text>
  //     <Text ml="30px" color="blue.400" fontWeight="bold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere, labore.</Text>
  //     <Box my="30px" p="20px" bg="orange">
  //       <Text color="white">This is a box</Text>
  //     </Box>
  //     <Box sx={boxStyles}>
  //       Hello, world!
  //     </Box>
  //   </Container>
  // )

    return (
      <SimpleGrid minChildWidth="300px">
        {tasks && tasks.map(task => (
          <Card key={task.id} borderTop="8px" borderColor="purple.400" bg="white">
            <CardHeader>
              <Flex gap={5}>
                <Avatar src={task.img}/>
                <Box>
                  <Heading as="h3" size="sm">{task.title}</Heading>
                  <Text>by {task.author}</Text>
                </Box>
              </Flex>

            </CardHeader>

            <CardBody color="gray.500">
              <Text>{task.description}</Text>
            </CardBody>

            <Divider borderColor="gray.200" />


            <CardFooter>
              <HStack>
                <Button variant="ghost" leftIcon={<ViewIcon />}>Watch</Button>
                <Button variant="ghost" leftIcon={<EditIcon />}>Comment</Button>
              </HStack>
            </CardFooter>
          </Card>

        ))}

      </SimpleGrid>


    )

}

export const tasksLoader = async () => {
  const res = await fetch('http://localhost:3000/tasks')

  return res.json()
}
