import { ChatIcon, CheckCircleIcon, EmailIcon, StarIcon, WarningIcon } from "@chakra-ui/icons";
import { Tab, TabList, TabPanel, TabPanels, Tabs, List, ListItem, ListIcon } from "@chakra-ui/react";

export default function Profile() {
  return (
    <Tabs mt="40px" p="20px" colorScheme="purple" variant="enclosed">
      <TabList>
        <Tab _selected={{ color: "white", bg: "purple.400"}}>Account Info</Tab>
        <Tab _selected={{ color: "white", bg: "purple.400"}}>Task History</Tab>
      </TabList>

      <TabPanels>
        <TabPanel>
          <List spacing={4}>
            <ListItem>
              <ListIcon as={EmailIcon} />
                Email: jane.smith@gmail.com
            </ListItem>
            <ListItem>
              <ListIcon as={ChatIcon} />
                lorem ipsum dolor sit amet
            </ListItem>
            <ListItem>
              <ListIcon as={StarIcon} />
                lorem ipsum dolor sit amet
            </ListItem>
          </List>
        </TabPanel>
        <TabPanel>
          <List spacing={4}>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="teal.400"/>
                  Lorem ipsum dolor sit amet consectetur
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="teal.400" />
                  Lorem ipsum dolor sit amet consectetur
              </ListItem>
              <ListItem>
                <ListIcon as={WarningIcon} color="red.400" />
                  Lorem ipsum dolor sit amet consectetur
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="teal.400" />
                  Lorem ipsum dolor sit amet consectetur
              </ListItem>
              <ListItem>
                <ListIcon as={WarningIcon} color="red.400" />
                  Lorem ipsum dolor sit amet consectetur
              </ListItem>



            </List>
        </TabPanel>
      </TabPanels>
    </Tabs>
  )
}


//amount of tabs in TabList should be how many panels created, first panel matches with first tab etc
